alias reload!='. ~/.zshrc'
alias fact="elinks -dump randomfunfacts.com | sed -n '/^| /p' | tr -d \|"
alias tree='tree -C'
      
alias e='emacsclient -t -a ""'
alias ec='emacsclient -c -a ""'
