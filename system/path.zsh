export PATH="$HOME/bin:$HOME/.rbenv/shims:/usr/local/bin:/usr/local/sbin:$HOME/.sfs:$ZSH/bin:$PATH"

export MANPATH="/usr/local/man:/usr/local/mysql/man:/usr/local/git/man:$MANPATH"

#Heroku toolbelt
export PATH="/usr/local/heroku/bin:$PATH"

# Brew-installed-npm binaries
export PATH="/usr/local/share/npm/bin:$PATH"
